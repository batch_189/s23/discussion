
/**
        OBJECTS
            An object is a data type that is used to present real world
            objects. It also a collaboration of related data and/or functionalities.

        creating object literal
        Syntax:
            let ObjectName = {
                keyA: valueA,
                keyB: ValueB,
            }
 */

            let cellphone = {
                name: "nokia 3210",
                manufacturedDate: 1999,
            }
            console.log("Result from creating objects");
            console.log(cellphone);
            console.log(typeof cellphone);
        
        
        // Creating objects using a constructor function
        /**
                Creates a reusable function to create a several objects that 
                have the sane data structure.
                This is useful for creating multiple instances/copies of an object.
        
                Syntax:
                    function ObjectName(valueA, valueB) {
                        this.KeyA = valueA,
                        this.keyB = valueB,
                    };
        
            // capitalize first letter in syntax constructor function
                    pagmarami ang ilalagay na objects with the same key use constructor function
         */
        
            function Laptop (name, model) {
                this.name = name;
                this.model = model
            };
        
            let laptop = new Laptop("MSI", 2003);
            console.log();
            console.log("Result of creating objects using object constructor");
            console.log(laptop);
        
            let myLaptop = new Laptop("Macbook Air", 2020);
            console.log(myLaptop);
        
        // Can use array to provide multiple key and value of a single variable.  
            let myLaptop1 = new Laptop(["Macbook Air", "heyeh"], [2020, 2901, 2901]);
            console.log(myLaptop1);
        
            let oldLaptop = Laptop ("Portal R2E", 1990);
            console.log();
            console.log("Result without the (new) key word");
            console.log(oldLaptop);
        
        // Creating empty objects
            let computer = {};
            let myComputer = new Object();
            console.log();
            console.log(computer);
            console.log(myComputer);
        
        // ACCESSING OBJECT PROPERTY
        // use dot notation for object and square notation bracket for array
        
        // Using the dot notation
            console.log();
            console.log("Result from dot notation: " + myLaptop.name);
        
        // Using the square bracket notation
            console.log("Result of square bracket notation: " + myLaptop["name"]);
        
        //  Accessing array objects
            let array = [laptop, myLaptop];
        
            console.log(array[0]["name"]);
            console.log(array[0].model);
        
        // Initializing/Adding/Deleting/Reassigning Object Property
            let car = {};
            console.log(car);
        
            car.name = "Honda Civic";
            console.log("Result from adding property using dot notation: ");
            console.log(car);
            console.log();
        
            car["model date"] = 2020;
            console.log(car);
        
        // Deleting object property
            delete car["model date"];
            console.log();
            console.log("Result from deleting object properties");
            console.log(car);
        
        // Reassigning object properties
            car.name = "tesla";
            console.log();
            console.log("Result from Reassigning property: ");
            console.log(car);
        
        // OBJECT METHODS
        /**
            A method is a function which is a properyt of an object. They are
            also function and one of the key differences they have is that
            methods are functions related to a specific object.
         */
        
            let person = {
                name: "John",
                talk: function(){
                    console.log("Hello! My name is " + this.name);
                },
            };
            console.log();
            console.log("Result from object methods: ");
            console.log(person);
            person.talk();
        
            person.walk = function() {
                console.log(this.name + " walked 25 steps forward");
            };
            console.log();
            person.walk()
        
        
            let friend = {
                firstName: "Nehemiah",
                lastName: "Ellorico",
                address: {
                    city: "Austin Texas",
                    country: "US",
                },
                emails: ["nejellorico@gmail.com", "nene@gmail.com"],
                introduce: function() {
                    console.log("Hello! My name is " + this.firstName + "" + this.lastName);
                },
            };
            friend.introduce();
            console.log();
        

// Real World Application Objects
/**
        Scenario:
            1. We would like to create a game that would have several pokemon
            interact with each other.
            2. Every pokemon would have the same set of stats, properties and functions.
 */

            let myPokemon = {
                name: "Pikachu",
                level: 3,
                health: 100,
                attack: 50,
                tackle: function(){
                    
                    console.log("This pokemon tackled targetPokemon ");
                    console.log("targetPokemon's health is now reduced to targetPokemonHealth");
                },
                faint: function(){
                    console.log("Pokemon fainted");
                },
            };
            console.log(myPokemon);
        
        // Creating an object constructor
            function Pokemon (name, level) {
        
                //properties
                this.name = name;
                this.level = level;
                this.health = 3 * level;
                this.attack = level;
        
                // methods
                this.tackle = function (target) {
                    
                    console.log(this.name + " tackled " + target.name);
                    console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
                    
                },
                this.faint = function() {
                    console.log(this.name + " fainted.");
                }
            }
        
            let pikachu = new Pokemon ("Pikachu", 16);
            let squirtle = new Pokemon ("Squirtle", 8); // 24
        
            console.log(pikachu);
            console.log(squirtle);
        
            pikachu.tackle(squirtle);
            console.log();
        